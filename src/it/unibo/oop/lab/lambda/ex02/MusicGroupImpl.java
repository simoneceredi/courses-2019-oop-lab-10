package it.unibo.oop.lab.lambda.ex02;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 */
public final class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    @Override
    public Stream<String> orderedSongNames() {
        return this.songs.stream()
                .map(s -> s.getSongName())
                .sorted();
    }

    @Override
    public Stream<String> albumNames() {
        return this.albums.keySet().stream();
    }

    @Override
    public Stream<String> albumInYear(final int year) {
        return this.albums.keySet().stream()
                .filter(s -> albums.get(s) == year);
    }

    @Override
    public int countSongs(final String albumName) {
        return (int) this.songs.stream()
                .filter(s -> s.getAlbumName().equals(Optional.of(albumName)))
                .count();
    }

    @Override
    public int countSongsInNoAlbum() {
        return (int) this.songs.stream()
                .filter(s -> !s.getAlbumName().isPresent())
                .count();
    }

    @Override
    public OptionalDouble averageDurationOfSongs(final String albumName) {
        return this.songs.stream()
                .filter(s -> s.getAlbumName().equals(Optional.of(albumName)))
                .mapToDouble(s -> s.getDuration())
                .average();
    }

    @Override
    public Optional<String> longestSong() {
        return this.songs.stream()
                .max((o1, o2) -> Double.compare(o1.getDuration(), o2.getDuration()))
                .map(s -> s.getSongName());
    }

    @Override
    public Optional<String> longestAlbum() {
         return this.songs.stream()
                 .filter(s -> s.getAlbumName().isPresent())
                 .collect(Collectors.groupingBy(s -> s.getAlbumName().get(), Collectors.summingDouble(Song::getDuration)))
                 .entrySet().stream()
                 .max((x, y) -> Double.compare(x.getValue(), y.getValue()))
                 .map(x -> x.getKey());
    }

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

    public static void main(final String[] args) {
        final String UNTITLED = "untitled";
        final String III = "III";
        MusicGroup lz;

        /**
         * Sets up the testing.
         */

        lz = new MusicGroupImpl();
        lz.addAlbum("I", 1969);
        lz.addAlbum("II", 1969);
        lz.addAlbum(III, 1970);
        lz.addAlbum(UNTITLED, 1971);
        lz.addSong("Dazed and Confused", Optional.of("I"), 6.5);
        lz.addSong("I Can't Quit You Baby", Optional.of("I"), 4.6);
        lz.addSong("Whole Lotta Love", Optional.of("II"), 5.5);
        lz.addSong("Ramble On", Optional.of("II"), 4.6);
        lz.addSong("Immigrant Song", Optional.of(III), 2.4);
        lz.addSong("That's the Way", Optional.of(III), 5.4);
        lz.addSong("Black Dog", Optional.of("untitled"), 4.9);
        lz.addSong("When the Levee Breaks", Optional.of("untitled"), 7.1);
        lz.addSong("Travelling Riverside Blues", Optional.empty(), 5.2);

        lz.longestAlbum();
    }

}
